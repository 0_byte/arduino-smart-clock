#include <Arduino.h>

#include <Wire.h>
#include "RTClib.h"
#include <LiquidCrystal_I2C.h>
#include <IRremote.h>
#include <dht.h>

//screen viewport
#define screenDefault 0;
#define screenAltern1 1; //alternative
#define screenAltern2 2; //third one

//pinout
const byte IR_PIN = 7;
const byte RELAY_PIN1 = 3;
const byte RELAY_PIN2 = 4;
const byte DHT11_PIN = 2;

//remote button keycodes
const int button1HexCode = 0xff30cf;
const int button2HexCode = 0xff18e7;
const int button3HexCode = 0xff7A85;
const int buttonDisplayHexCode = 0xffe21d;

dht DHT;
RTC_DS1307 RTC;
LiquidCrystal_I2C lcd(0x3F, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
IRrecv irrecv(IR_PIN);
decode_results results;

bool relay1State = false;
bool relay2State = false;
bool alarm1State = false;
bool isDisplayOn = true;

byte currentScreen =  screenDefault;
int tempo = 0;
int hum = 0;

// counter for skipping loops in main loop function
int dht_counter = 0;

//custom chars for LCD
uint8_t celsius_char[8] = {0x06,0x09,0x09,0x06,0x00,0x00,0x00,0x00}; 
uint8_t thermometer_chars[8] = {0x04,0x0a,0x0a,0x0a,0x0e,0x1f,0x1f,0x0e}; 
uint8_t thermometer_chars2[8] = {0x04,0x0a,0x0a,0x0a,0x0a,0x11,0x1f,0x0e}; 
uint8_t clock_chars[8] = {0x00,0x00,0x0e,0x15,0x17,0x11,0x0e,0x00}; 

uint8_t rellay1chars_closed[8] = {0x00,0x04,0x0c,0x04,0x04,0x04,0x0e,0x00}; 
uint8_t rellay1chars_open[8] = {0x1f,0x15,0x1d,0x15,0x15,0x15,0x01f,0x1f}; 

uint8_t rellay2chars_closed[8] = {0x00,0x04,0x0a,0x02,0x0c,0x08,0x0e,0x00};
uint8_t rellay2chars_open[8] = {0x1f,0x15,0x1b,0x13,0x1d,0x19,0x1f,0x1f}; 

uint8_t displayBell[8] = {0x04,0x0E,0x11,0x11,0x11,0x11,0x1F,0x04}; 

int year = 0;
int month = 0;
int day = 0;
int hour = 0;
int min = 0;
int sec = 0; 

void setup () {

  Serial.begin(9600);

  pinMode(RELAY_PIN1, OUTPUT);
  pinMode(RELAY_PIN2, OUTPUT);
  digitalWrite(RELAY_PIN1, HIGH);
  digitalWrite(RELAY_PIN2, HIGH);
  
  Wire.begin();
  RTC.begin();
  irrecv.enableIRIn();

  if (! RTC.isrunning()) {
    Serial.println("RTC problem");
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }

  lcd.begin(16,2); 
  
  for(int i = 0; i< 3; i++)
  {
    lcd.backlight();
    delay(150);
    lcd.noBacklight();
    delay(150);
  }
  lcd.backlight();  
  
  lcd.setCursor(0,0);
  lcd.print("Initialization");

  delay(100);
  for(int i=5; i<10; i++){
    lcd.setCursor(i,1);
    lcd.print(".");   
    delay(150); 
  }

  lcd.clear();

  lcd.createChar(1, celsius_char); 
  lcd.createChar(2, rellay1chars_closed); 
  lcd.createChar(3, rellay2chars_closed); 
  lcd.createChar(4, rellay1chars_open);
  lcd.createChar(5, rellay2chars_open); 
  lcd.createChar(6, displayBell);     
  lcd.createChar(7, thermometer_chars2);   
  lcd.createChar(8, clock_chars);      

  lcd.setCursor(0,0);
  lcd.write(byte(7));
  
  lcd.setCursor(4,0);
  lcd.write(byte(1));
  lcd.setCursor(5,0);
  lcd.print("C");

  lcd.setCursor(7,0);
  lcd.print("h:");
  lcd.setCursor(11,0);
  lcd.print("%");  

//alrm
//  lcd.setCursor(13,0);
//  lcd.write(byte(6));

  lcd.setCursor(14,0);
  lcd.write(byte(2));
  lcd.setCursor(15,0); 
  lcd.write(byte(3));

  lcd.setCursor(0,1);
  lcd.write(byte(8));
}

void loop () {

    handleIRSig();
    showTempo();
    
    DateTime now = RTC.now();
//    year = (int) now.year();
    month = (int) now.month();  
    day = (int) now.day();  
    hour =  (int) now.hour() ;  
    min =  (int) now.minute();
    sec =  (int) now.second(); 

    printTime();
    showTime();

    //wait and skip few cycles for having wright dht sensor values...e.g. about 2 seconds
    if(dht_counter == 2){
      handleDHT();
      dht_counter = 0;
    }
    
    dht_counter++;
    delay(1000);
}

void showTempo(){
   lcd.setCursor(2,0);
   lcd.print(tempo);
   
   lcd.setCursor(9,0);
   lcd.print(hum);
}

void handleDHT(){
  int chk = DHT.read11(DHT11_PIN);
  if(chk == DHTLIB_OK){
    tempo = (int) DHT.temperature;
    hum = (int) DHT.humidity;
    
    Serial.print(DHT.humidity, 1);
    Serial.print(",\t");
    Serial.println(DHT.temperature, 1);        
  }
}

//change state for relay and display it
void changeState(int pin, bool &state, int charHIGHT, int charLOW, int posX, int posY){
  lcd.setCursor(posX, posY);
  if(!state){
    digitalWrite(pin, LOW);
    lcd.write(byte(charHIGHT)); 
  }else{
    digitalWrite(pin, HIGH);
    lcd.write(byte(charLOW)); 
  }
  state = !state;
}

// hanle ir requests
void handleIRSig(){
  if (irrecv.decode(&results)) {
      int resultCode = results.value;
      Serial.print("Obtain IR-code: ");
      Serial.println(results.value, HEX);
      irrecv.resume(); // Receive the next value

      switch(resultCode){        
          case button1HexCode:
            Serial.println("Relay 1 triggered");
            changeState(RELAY_PIN1, relay1State, 4, 2, 14, 0);
            break;
          case button2HexCode:
            Serial.println("Relay 2 triggered");
            changeState(RELAY_PIN2, relay2State, 5, 3, 15, 0);
            break;
          case button3HexCode:
            // reset to default state
            Serial.println("shut em down");           
            setRelayDefaultState();
            break;
          case buttonDisplayHexCode:
            changeDisplayState(isDisplayOn);        
            break;          
      }     
    }
}

void setRelayDefaultState(){
    digitalWrite(RELAY_PIN1, HIGH);
    digitalWrite(RELAY_PIN2, HIGH);
    relay1State = false;
    relay2State = false;
        
    lcd.setCursor(14,0);
    lcd.write(byte(2));          
    lcd.setCursor(15,0);
    lcd.write(byte(3));
}

void printTime(){
    Serial.print(hour);
    Serial.print(":");
    Serial.print(min);
    Serial.print(":");
    Serial.print(sec);
    Serial.println();
}

void showTime(){
    lcd.setCursor(2,1);
    getLeadsWithzero(hour);
    lcd.print(hour);
    
    lcd.setCursor(4,1);
    lcd.print(":");
    
    lcd.setCursor(5,1);
    getLeadsWithzero(min);
    lcd.print(min);
    
    lcd.setCursor(7,1);
    lcd.print(":");
    
    lcd.setCursor(8,1);
    getLeadsWithzero(sec);
    lcd.print(sec);  

    lcd.setCursor(11,1);
    getLeadsWithzero(day);
    lcd.print(day);

    lcd.setCursor(13,1);
    lcd.print("/");
    
    lcd.setCursor(14,1);
    getLeadsWithzero(month);
    lcd.print(month);
}

int getLeadsWithzero(int num){
  if(num < 10) {
      lcd.print(0);
   }
}

void changeDisplayState(bool &state){
  if(state){
    lcd.off();
  }else{
    lcd.on();
  }
  state = !state;
}

